package com.mercury.test;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import jxl.read.biff.BiffException;

public class RegressionTestScript {
	
	
	@Test(enabled=false)
	public void verifiedValidLogin() throws InterruptedException, IOException
	{
		MethodRepository obj=new MethodRepository();
		obj.browserAppLaunch();
		obj.validLogin();
		Assert.assertTrue(obj.verifiedValidLogin(), "Tested");
		
		obj.appClose();
	}
	
	
	@Test(enabled=true)
	public void readExcel() throws BiffException, IOException
	{
		MethodRepository obj1=new MethodRepository();
		    			 obj1.excelReading();
	}

}
