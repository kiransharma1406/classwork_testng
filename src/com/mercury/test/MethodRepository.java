package com.mercury.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class MethodRepository {WebDriver driver;

	
	public void browserAppLaunch()
	{
		//System.out.println("start editing");
		System.setProperty("webdriver.chrome.driver", "F:\\Palliumskills\\AutomationTesting\\Tools\\NewChrom\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		driver.manage().window().maximize();
	}
	
	public void validLogin() throws InterruptedException
	{
		
		WebElement uName=driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");
		WebElement uPassword=driver.findElement(By.xpath("//input[@name='password']"));
		uPassword.sendKeys("dasd");
		WebElement uSign=driver.findElement(By.xpath("//input[@name='login']"));
		uSign.click();
		
		Thread.sleep(5000);
	}
	
	public boolean verifiedValidLogin()
	{
		String expectedtitle= "Find a Flight: Mercury Tours:";
		String actualtitle= driver.getTitle();
		
		if(expectedtitle.equalsIgnoreCase(actualtitle))
		{
			//System.out.println("PASS");
			return true;
		}
		else
		{
			//System.out.println("FAIL");
			return false;
		}
	}
	
	public void appClose()
	{
		driver.close();
	}
	
	public void excelReading() throws BiffException, IOException
	{
		File fis=new File("F:\\Palliumskills\\AutomationTesting\\UserProfile.xls");
		FileInputStream fistrm = new FileInputStream(fis);
		Workbook wb = Workbook.getWorkbook(fistrm); 
		Sheet sh = wb.getSheet("Sheet1");
		int row = sh.getRows();
	    int col = sh.getColumns();
	    for (int i=1;i< row;i++)
	    {
	        for(int j=0;j<col;j++)
	        {
	           System.out.println(sh.getCell(j,i).getContents());
	        }
	    }	
	}
}
